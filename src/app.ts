
export class App {
  router: any;
  appTitle:string;
  configureRouter(config, router) {
    this.router = router;
    config.title = 'Aurelia';
    config.map([
      { route: '',              moduleId: 'modules/modules/modules',   title: 'Modules'},
      { route: 'modules',              moduleId: 'modules/modules/modules',   title: 'Modules'},
      { route: 'customers',              moduleId: 'modules/customers/customers',   title: 'Customers'},
      { route: 'surveys',              moduleId: 'modules/surveys/surveys',   title: 'Surveys'}
    ]);

    this.appTitle = config.title;
  }
}
