import {inject} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {ModulesService} from './modules-service';

@inject(ModulesService)
export class Modules {
  
  modulesService: any;
  modules: any[];
  constructor(ModulesService){
    this.modulesService = ModulesService;
    this.modules = this.modulesService.getModules();
  }

  selectModule(idx) {
    console.log(idx);
  }
}
